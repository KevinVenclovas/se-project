#include "sheet.h"
#include "User.h";

Sheet::Sheet(int id,
             int dozent_id,
             QString title,
             QString description,
             QStringList keywords,
             ProjectStatus::EnumProjectStatus status,
             QString editor,
             QString supervisor,
             Subject::EnumSubject subject,
             QString semester,
             MainSubject::EnumMainSubject mainSubject,
             QString company,
             SheetType::EnumSheetType sheet,
             QString start,
             QString end
             ) {
    this->id = id;
    this->dozent_id = dozent_id;
    this->title = title;
    this->description = description;
    this->keywords = keywords;
    this->status = status;
    this->editor = editor;
    this->supervisor = supervisor;
    this->subject = subject;
    this->semester = semester;
    this->mainSubject = mainSubject;
    this->company = company;
    this->sheet = sheet;
    this->start = start;
    this->end = end;
}

Sheet::Sheet(int did,SheetType::EnumSheetType sheet){
    this->dozent_id = did;
    this->id = NULL;
    this->sheet = sheet;
}
Sheet::Sheet(int did){
    this->dozent_id = did;
}
QString Sheet::isValid(){

    if(this->title == "" ||
       this->description == ""||
       this->editor == ""||
       this->supervisor == ""||
       this->status == -1  ||
       this->sheet == -1 ||
       this->subject == -1
            ){

        return "Bitte füllen Sie alle notwenigen Felder aus.";


    }else{
        return NULL;
    }


}
