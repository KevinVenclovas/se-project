/** \file
 *  UI für den Login.
 */
#ifndef LOGIN_H
#define LOGIN_H

#include <QMainWindow>

namespace Ui {
class Login;
}

class Login : public QMainWindow
{
    Q_OBJECT

public:
    explicit Login(QWidget *parent = 0);
    ~Login();
    void login();
    void login_guest();

private slots:
    void on_btn_login_clicked();

    void on_btn_login_guest_clicked();

    void on_btn_exit_clicked();

private:
    Ui::Login *ui;
    void setButton(bool status);
};

#endif // LOGIN_H
