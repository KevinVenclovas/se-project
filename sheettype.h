/** \file
 *  Enumeration für alle verfügbaren Dokumenttypen.
 */
#ifndef SHEETTYPE_H
#define SHEETTYPE_H

#include <QObject>
#include <QMetaEnum>
class SheetType : public QObject
{
    Q_OBJECT
    Q_ENUMS(EnumSheetType)
public:

public:
    explicit SheetType (QObject *parent = 0);

    enum EnumSheetType{
        ARBEIT,
        PROJECTARBEIT,
        ABSCHLUSSARBEIT
    };


};

#endif // SHEETTYPE_H
