/** \file
 *  Enumeration für alle verfügbaren Usertypen.
 */
#ifndef USERTYPE_H
#define USERTYPE_H

#include <QObject>
#include <QMetaEnum>
class UserType : public QObject
{
    Q_OBJECT
    Q_ENUMS(EnumUsertype)
public:

public:
    explicit UserType (QObject *parent = 0);

    enum EnumUsertype{
        ADMIN,
        DOZENT,
        GUEST
    };


};

#endif // USERTYPE_H
