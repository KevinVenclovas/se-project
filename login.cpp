#include "login.h"
#include "ui_login.h"
#include "UserType.h"
#include "User.h"
#include "MainWindow.h"
#include "DatabaseService.h"
#include "Sheet.h"
#include "ShowSheet.h"
Login::Login(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
}

Login::~Login()
{
    delete ui;
}

/**
 *  \brief Login eines \link User \endlink.
 *
 *   Ladet daten aus Inputfelder und übergibt diese dem \link DatabaseService \endlink.
 *   Wird ein \link User \endlink zurückgeliefter so wird das Hauptfenster geöffnet.
 *   Falls nicht so ist der Login nicht richtig.
 */
void Login::login(){
    DatabaseService *db = new DatabaseService();

    User* u = db->checkLogin(this->ui->ip_email->text(),this->ui->ip_password->text());

    if(u){
        MainWindow* w = new MainWindow();
        w->setUpAndShow(u);
        this->close();
    }
}

/**
 *  \brief Login eines Gasts.
 *
 *   Erzeugt Gast-\link User \endlink und öffnet das Hauptfenster.
 */
void Login::login_guest(){
    User* u = new User(0,"Gast","Gast",UserType::GUEST);
    MainWindow* w = new MainWindow();
    w->setUpAndShow(u);
    this->close();
}

void Login::on_btn_login_clicked()
{
       this->login();
}

void Login::on_btn_login_guest_clicked()
{   this->login_guest();

}

void Login::on_btn_exit_clicked()
{
    this->close();

}

