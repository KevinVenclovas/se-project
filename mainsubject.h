/** \file
 *  Enumeration für alle verfügbaren Schwerpunkte.
 */
#ifndef MAINSUBJECT_H
#define MAINSUBJECT_H

#include <QObject>
#include <QMetaEnum>
class MainSubject : public QObject
{
    Q_OBJECT
    Q_ENUMS(EnumMainSubject)
public:

public:
    explicit MainSubject (QObject *parent = 0);

    enum EnumMainSubject{
        IN_PROJECT,
        IS_PROJECT,
        MI_PROJECT,
        SE_PROJECT,
        MASTER_PROJECT,
    };


};

#endif // MAINSUBJECT_H
