/** \file
 *  UI für das Auswählen eines Dokumenttypen für das Erstellen .
 */
#ifndef SELECTSHEETTYPE_H
#define SELECTSHEETTYPE_H

#include <QMainWindow>
#include "MainWindow.h"

namespace Ui {
class SelectSheetType;
}

class SelectSheetType : public QMainWindow
{
    Q_OBJECT

public:
    explicit SelectSheetType(QWidget *parent = 0);
    ~SelectSheetType();
    MainWindow* parent;
    User* user;
    void setUpAndShow(User* u,MainWindow* parent);
private slots:
    void on_btn_arbeit_clicked();

    void on_btn_Project_clicked();

    void on_btn_abschlussarbeit_clicked();

    void on_btn_exit_clicked();

private:
    Ui::SelectSheetType *ui;
};

#endif // SELECTSHEETTYPE_H
