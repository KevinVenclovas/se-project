/** \file
 * Dokument Object .
 */
#ifndef SHEET_H
#define SHEET_H

#include "QString.h"
#include "ProjectStatus.h"
#include "Subject.h"
#include "MainSubject.h"
#include "SheetType.h"
#include "ListItem.h"
#include "User.h";

class Sheet : public ListItem
{
public:
    int id;
    int dozent_id;
    QString title;
    QString description;
    QStringList keywords;
    ProjectStatus::EnumProjectStatus status;
    QString editor;
    QString supervisor;
    Subject::EnumSubject subject;
    QString semester;
    MainSubject::EnumMainSubject mainSubject;
    QString company;
    SheetType::EnumSheetType sheet;
    QString start;
    QString end;
    Sheet(int id,
          int dozent_id,
          QString title,
          QString description,
          QStringList keywords,
          ProjectStatus::EnumProjectStatus status,
          QString editor,
          QString supervisor,
          Subject::EnumSubject subject,
          QString semester,
          MainSubject::EnumMainSubject mainSubject,
          QString company,
          SheetType::EnumSheetType sheet,
          QString start,
          QString end);
    Sheet(int did,SheetType::EnumSheetType);
    Sheet(int did);
    QString isValid();

protected:
    virtual QString zeigeName()
    {
        return title;
    }

};

#endif // SHEET_H
