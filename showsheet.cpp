#include "showsheet.h"
#include "ui_showsheet.h"
#include "Sheet.h"
#include "QPushButton.h"
#include "Subject.h"
#include "MainSubject.h"
#include "SheetType.h"
#include "ProjectStatus.h"
#include "QMessageBox.h"
#include "DatabaseService.h"
#include "MainWindow.h"

ShowSheet::ShowSheet(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ShowSheet)
{
    ui->setupUi(this);
}

void ShowSheet::drawButtonChange() {
    QPushButton *exit = new QPushButton("Abbrechen");
    QPushButton *confirm = new QPushButton("Speichern");

    ui->lay_sheet_btn->addWidget(exit);
    ui->lay_sheet_btn->addWidget(confirm);

    QObject::connect(exit, SIGNAL(clicked()), this, SLOT(exit()));
    QObject::connect(confirm, SIGNAL(clicked()), this, SLOT(confirm()));

    this->show();
}


void ShowSheet::drawButtonOpen() {
    QPushButton *leave = new QPushButton("Schließen");
    ui->lay_sheet_btn->addWidget(leave);
    QObject::connect(leave, SIGNAL(clicked()), this, SLOT(leave()));
    this->show();
}
void ShowSheet::leave() {
    this->close();
}

void ShowSheet::exit() {
    this->close();
}

void ShowSheet::confirm() {


    int id = currentSheet->id;
    currentSheet->title = ui->txt_title->text();
    currentSheet->description= ui->txt_description->toPlainText();

    QString keywords_without_space = ui->txt_keywords->text().replace(" ","");
    QStringList keywords = keywords_without_space.split( "," );

    currentSheet->keywords = keywords;


    currentSheet->editor = ui->txt_editor->text();
    currentSheet->supervisor = ui->txt_supervisor->text();
    currentSheet->semester = ui->txt_semester->text();
    currentSheet->company = ui->txt_company->text();
    currentSheet->start = ui->date_start->text();
    currentSheet->end = ui->date_end->text();

    currentSheet->sheet = currentSheet->sheet;
    currentSheet->status = static_cast<ProjectStatus::EnumProjectStatus>(ui->cmb_status->currentIndex());
    currentSheet->subject = static_cast<Subject::EnumSubject>(ui->cmb_subject->currentIndex());
    currentSheet->mainSubject = static_cast<MainSubject::EnumMainSubject>(ui->cmb_main_subject->currentIndex());



    QString s = currentSheet->isValid();

    if(s == NULL){
        DatabaseService *db = new DatabaseService();
        bool result = db->saveSheet(currentSheet);


        if(result){
            QMessageBox::StandardButton reply;
              reply = QMessageBox::information(NULL,"Info","Ihr Eintrag wurde in die Datenbank aufgenommen",QMessageBox::Yes);

            this->close();
        }else{
            QMessageBox::StandardButton reply;
              reply = QMessageBox::warning(NULL,"Info","Versuchen Sie es nochmals. Gegebenfalls überprüfen Sie Ihre Eingaben",QMessageBox::Yes);
        }
    }else{
        QMessageBox::StandardButton reply;
          reply = QMessageBox::warning(NULL,"Info",s,QMessageBox::Yes);
    }
    this->parent->updateListView(currentSheet);

}



void ShowSheet::setUpAndShow(MainWindow* parent,bool editable,Sheet* sheet) {
    this->currentSheet = sheet;
    this->parent = parent;
    if(sheet){

        this->ui->txt_title->setText(sheet->title);
        this->ui->txt_description->setText(sheet->description);

        QString keywords = sheet->keywords.join(",");
        this->ui->txt_keywords->setText(keywords);


        this->ui->cmb_status->setCurrentIndex((sheet->status));
        this->ui->txt_editor->setText(sheet->editor);
        this->ui->txt_supervisor->setText(sheet->supervisor);
        this->ui->cmb_subject->setCurrentIndex((sheet->subject));
        this->ui->date_start->setDate( QDate::fromString(sheet->start, "dd-MM-yyyy"));
        this->ui->date_end->setDate( QDate::fromString(sheet->end, "dd-MM-yyyy"));


        switch (sheet->sheet) {
            case SheetType::ARBEIT:
                this->ui->txt_semester->setDisabled(true);
                this->ui->cmb_main_subject->setDisabled(true);
                this->ui->date_end->setDisabled(true);
                this->ui->date_start->setDisabled(true);
                this->ui->txt_company->setDisabled(true);
                break;

            case SheetType::PROJECTARBEIT:
                this->ui->txt_semester->setText(sheet->semester);
                this->ui->cmb_main_subject->setCurrentIndex(sheet->mainSubject);
                this->ui->txt_company->setDisabled(true);

                break;
            case SheetType::ABSCHLUSSARBEIT:
                this->ui->txt_company->setText(sheet->company);
                this->ui->cmb_main_subject->setDisabled(true);
                this->ui->txt_semester->setDisabled(true);
                break;
            default:
                break;

        }
    }

    if(sheet->id == NULL){
         this->ui->cmb_status->setCurrentIndex(-1);
         this->ui->cmb_main_subject->setCurrentIndex(-1);
         this->ui->cmb_subject->setCurrentIndex(-1);
    }



    if (editable == false) {
        this->ui->cmb_main_subject->setDisabled(true);
        this->ui->cmb_status->setDisabled(true);
        this->ui->cmb_subject->setDisabled(true);
        this->ui->txt_company->setDisabled(true);
        this->ui->txt_description->setDisabled(true);
        this->ui->txt_editor->setDisabled(true);
        this->ui->txt_keywords->setDisabled(true);
        this->ui->txt_supervisor->setDisabled(true);
        this->ui->date_start->setDisabled(true);
        this->ui->date_end->setDisabled(true);
        this->ui->txt_title->setDisabled(true);
        this->ui->txt_semester->setDisabled(true);

        this->drawButtonOpen();
    }
    else {
        this->drawButtonChange();
    }

    this->show();
}


