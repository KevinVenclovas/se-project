/** \file
 *  Enumeration für alle verfügbaren Studiengänge.
 */
#ifndef SUBJECT_H
#define SUBJECT_H

#include <QObject>
#include <QMetaEnum>
class Subject : public QObject
{
    Q_OBJECT
    Q_ENUMS(EnumSubject)
public:

public:
    explicit Subject (QObject *parent = 0);

    enum EnumSubject{
        IN,
        IS,
        MI,
        SE,
        MASTER
    };


};

#endif // SUBJECT_H
