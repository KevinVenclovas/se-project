/** \file
 *  Service für alle Aktionen, welche eine Verbindung zur Datenbank benötigen.
 */
#ifndef DATABASESERVICE_H
#define DATABASESERVICE_H
#include "User.h"
#include "QSqlDatabase.h"
#include "Sheet.h"
class DatabaseService
{
public:
    DatabaseService();
    ~DatabaseService();
    bool insertDozent(User* u);
    bool deleteDozent(User* u);
    bool changeDozentPassword(User* u,QString newpw,QString oldpw,bool needoldpw);
    bool saveSheet(Sheet* s);
    bool deleteSheet(Sheet* s);

    std::vector<ListItem*> loadSheets(QStringList list);
    std::vector<ListItem*> loadUsers();
    User* checkLogin(QString id, QString password);

private:
    QSqlDatabase dbConnect;
};

#endif // DATABASESERVICE_H
