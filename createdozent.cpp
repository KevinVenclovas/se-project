#include "createdozent.h"
#include "ui_createdozent.h"
#include "User.h"
#include "QMessageBox.h"
#include "DatabaseService.h"
CreateDozent::CreateDozent(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CreateDozent)
{
    ui->setupUi(this);
}

CreateDozent::~CreateDozent()
{
    delete ui;
}


/**
 *  \param parent Zeiger auf das \link MainWindow \endlink.
 *  Setzt die Verknüpfung zum \link MainWindow \endlink und zeigt Window an.
 */
void CreateDozent::setUpAndShow(MainWindow* parent)
{
    this->parent = parent;
    this->show();
}
/**
 *  Überprüft Eingaben. Erzeugt neues \link User \endlink Object mit angegebenen Daten.
 */
void CreateDozent::createDozent(){
    if(this->ui->ip_email->text().length()==0 || this->ui->ip_name->text().length()==0){

        QMessageBox::StandardButton reply;
          reply = QMessageBox::information(this,"Info","Bitte alle notwendigen Felder vollständig ausfüllen",QMessageBox::Yes);

    }else{
         User* u = new User(0,this->ui->ip_email->text(),this->ui->ip_name->text(),UserType::DOZENT);

         DatabaseService *db = new DatabaseService();
         bool result = db->insertDozent(u);
        if(result){
            QMessageBox::StandardButton reply;
              reply = QMessageBox::information(this,"Info","Dozent Erfolgreich erstellt",QMessageBox::Yes);
              parent->updateListView(u);
            this->close();
        }else{
            QMessageBox::StandardButton reply;
              reply = QMessageBox::information(this,"Info","Dozent konnte nicht erstellt werden. Möglicherweise exisitiert ein Dozent mit der Email bereits",QMessageBox::Yes);
        }
    }
}


void CreateDozent::on_btn_ok_clicked()
{
    this->createDozent();
}

void CreateDozent::on_btn_exit_clicked()
{
    this->close();
}
