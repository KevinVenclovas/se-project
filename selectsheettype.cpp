#include "selectsheettype.h"
#include "ui_selectsheettype.h"
#include "Sheet.h";
#include "ShowSheet.h";
SelectSheetType::SelectSheetType(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SelectSheetType)
{
    ui->setupUi(this);
}

SelectSheetType::~SelectSheetType()
{
    delete ui;
}

void SelectSheetType::setUpAndShow(User* u,MainWindow* parent){
    this->parent = parent;
    this->user = u;
    this->show();
}

void SelectSheetType::on_btn_arbeit_clicked()
{
    Sheet* sheet = new Sheet(this->user->id,SheetType::ARBEIT);
    ShowSheet* s = new ShowSheet();
    s->setUpAndShow(this->parent,true,sheet);
    this->close();
}

void SelectSheetType::on_btn_Project_clicked()
{
    Sheet* sheet = new Sheet(this->user->id,SheetType::PROJECTARBEIT);
    ShowSheet* s = new ShowSheet();
    s->setUpAndShow(this->parent,true,sheet);
     this->close();
}

void SelectSheetType::on_btn_abschlussarbeit_clicked()
{
    Sheet* sheet = new Sheet(this->user->id,SheetType::ABSCHLUSSARBEIT);
    ShowSheet* s = new ShowSheet();
    s->setUpAndShow(this->parent,true,sheet);
    this->close();
}

void SelectSheetType::on_btn_exit_clicked()
{
    this->close();
}
