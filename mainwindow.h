/** \file
 *  UI Mainwindow.
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "User.h"
#include "QStringListModel.h"
#include "ListItem.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setUpAndShow(User *user);
    std::vector<ListItem*> listOfItems;
    void updateListView(ListItem* item);
private:
    Ui::MainWindow *ui;
    QStringListModel *model;
    User* currentuser;

    // Funktionen der Button generierung je nach Typ
    void drawDozentView();
    void drawAdminView();
    void drawGuestView();

public slots:
    void createDozent();
    void createSheet();
    void changeSheet();
    void openSheet();
    void deleteSheet();
    void deleteDozent();
    void changePasswordDozent();
    void changePasswordAdmin();
private slots:
    void on_btn_search_clicked();
    void on_btn_reset_clicked();
    void logout();

};

#endif // MAINWINDOW_H
