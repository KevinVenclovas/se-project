/** \file
 *  Oberklasse für die Verwaltung von unterschiedlichen Objekten in einer Listview.
 */
#ifndef LISTITEM_H
#define LISTITEM_H
#include "QString.h"

class ListItem
{
public:
    virtual QString zeigeName() = 0;

};

#endif // LISTITEM_H
