/** \file
 * UI für das Anzeigen eines Dokuments .
 */
#ifndef SHOWSHEET_H
#define SHOWSHEET_H

#include <QMainWindow>
#include "Sheet.h"
#include "MainWindow.h";
namespace Ui {
class ShowSheet;
}

class ShowSheet : public QMainWindow
{
    Q_OBJECT

public:
    explicit ShowSheet(QWidget *parent = 0);
    void setUpAndShow(MainWindow* parent,bool editable,Sheet* s);

private:
    Sheet* currentSheet;
    Ui::ShowSheet *ui;
    MainWindow* parent;
private slots:
    void exit();
    void confirm();
    void leave();
    void drawButtonChange();
    void drawButtonOpen();

};

#endif // SHOWSHEET_H
