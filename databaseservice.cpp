#include "databaseservice.h"
#include "User.h"
#include "QSqlDatabase.h"
#include "QSqlQuery.h"
#include "QMessageBox.h"
#include "QDebug.h"
#include "QSqlError.h"
#include "PasswordService.h"
#include "UserType.h"
#include "Sheet.h"

/**
 *  \brief Konstruktor.
 *
 *  Initalisiert eine Datenbankverbindung.
 */
DatabaseService::DatabaseService()
{
    this->dbConnect = QSqlDatabase::addDatabase("QSQLITE");


    if(!this->dbConnect.open()){
        QMessageBox::StandardButton reply;
          reply = QMessageBox::warning(NULL,"Warnung","Verbindung zur Datenbank konnte nicht hergestellt werden",QMessageBox::Yes);
    }

}

DatabaseService::~DatabaseService()
{
    this->dbConnect.close();
}

/**
 *  \brief Speichert einen \link User \endlink in die Datenbank.
 *  \param u \link User \endlink, welcher gespeichert werden soll.
 *
 *   Erzeugt SQL Befehl für das Speichern des \link User \endlink und führt diesen aus.
 */
bool DatabaseService::insertDozent(User* u) {

    if(!this->dbConnect.isOpen()){
        return false;

    }else{

        QSqlQuery query(this->dbConnect);

        query.prepare("INSERT INTO User (EMAIL,NAME,PASSWORD,ACCOUNTTYPE) VALUES (:email,:name,:password,:accounttype)");
        query.bindValue(":email", u->email);
        query.bindValue(":name", u->name);

        PasswordService* p = new PasswordService();
        QString s = p->GetRandomPassword(8);

        query.bindValue(":password", s);
        query.bindValue(":accounttype", UserType::DOZENT);

        if(query.exec()){
            u->id = query.lastInsertId().toInt();
            return true;
        }

    }

    return false;
}

/**
 *  \brief Löscht einen \link User \endlink aus der Datenbank.
 *  \param u \link User \endlink, welcher gelöscht werden soll.
 *
 *   Erzeugt SQL-Befehl für das Löschen des \link User \endlink und führt diesen aus.
 */
bool DatabaseService::deleteDozent(User* u) {

    if(!this->dbConnect.isOpen()){
        return false;

    }else{

        QSqlQuery query(this->dbConnect);
        query.prepare("DELETE FROM User WHERE EMAIL = :email");
        query.bindValue(":email", u->email);
        if(query.exec()){

            QMessageBox::StandardButton reply;
              reply = QMessageBox::information(NULL,"Info","Dokument erfolgreich gelöscht",QMessageBox::Yes);
            return true;
        }
    }
    QMessageBox::StandardButton reply;
    reply = QMessageBox::warning(NULL,"Warnung","Fehler beim löschen aufgetreten",QMessageBox::Yes);
    return false;



 return true;
}

/**
 *  \brief Ändert das Passwort eines \link User \endlink.
 *  \param u \link User \endlink, welcher gelöscht werden soll.
 *  \param newpw Neues passwort für den \link User \endlink.
 *  \param oldpw Altes passwort des \link User \endlink.
 *  \param needoldpw status ob altes passwort für das ändern benbötigt wird.
 *
 *  Erzeugt SQl-Befehl für das Updaten des \link User \endlink passwortes.
 *  Passwort wird dann geändert wenn das alte Passwort mit dem in der Datenbank übereinstimmt.
 *  Ist needoldpw false, so wird das alte Passwort nicht überprüft.
 */
bool DatabaseService::changeDozentPassword(User* u, QString newpw,QString oldpw,bool needoldpw) {

    if(!this->dbConnect.isOpen()){
        return false;

    }else{

        QSqlQuery query(this->dbConnect);

        if(needoldpw){
           query.prepare("UPDATE User SET PASSWORD = :password WHERE EMAIL = :email AND PASSWORD = :oldpw");
           query.bindValue(":oldpw", oldpw);
        }else{
            query.prepare("UPDATE User SET PASSWORD = :password WHERE EMAIL = :email");
        }

        query.bindValue(":email", u->email);
        query.bindValue(":password", newpw);

        if(query.exec()){

            if(query.numRowsAffected()==0){
                QMessageBox::StandardButton reply;
                reply = QMessageBox::information(NULL,"Info","Altes Passwort stimmt nicht überrein",QMessageBox::Yes);
                 return true;
            }else{
                QMessageBox::StandardButton reply;
                reply = QMessageBox::information(NULL,"Info","Passwort wurde geändert",QMessageBox::Yes);
                 return false;
            }
        }
    }


    QMessageBox::StandardButton reply;
    reply = QMessageBox::warning(NULL,"Warnung","Passwort wurde nicht geändert.Bitte Versuchen Sie es erneut",QMessageBox::Yes);
    return false;


}
/**
 *  \brief Checkt Login.
 *  \param email Email Adresse des \link User \endlink.
 *  \param password Passwort des \link User \endlink.
 *
 *  Überprüft die User Datenbank auf übereinstimmung der Parameter.
 *   \return \link User \endlink
 */
User* DatabaseService::checkLogin(QString email, QString password) {

    if(!this->dbConnect.isOpen()){
        return false;

    }else{

        QSqlQuery query(this->dbConnect);

        if(query.exec("SELECT EMAIL,NAME,ACCOUNTTYPE,ID  FROM User WHERE EMAIL = '"+email+"' AND PASSWORD = '"+password+"'")){

            if (query.next())
            {
                int id = query.value(3).toInt();
                QString email = query.value(0).toString();
                QString name = query.value(1).toString();
                int usertype = query.value(2).toInt();
                UserType::EnumUsertype type = static_cast<UserType::EnumUsertype>(usertype);

                User* u = new User(id,email,name, type);
                return u;

            }

        }
    }

    QMessageBox::StandardButton reply;
      reply = QMessageBox::information(NULL,"Info","Email oder Passwort falsch",QMessageBox::Yes);

    return NULL;

}

/**
 *  \brief Speichert Dokument in die Datenbank.
 *  \param s Dokument Object .
 *
 *  Speichert oder Updatet DOkument in der Datenbank.
 *   \return bool
 */
bool DatabaseService::saveSheet(Sheet *s) {
    bool transaction_success = true;
    if(!this->dbConnect.isOpen()){
         transaction_success = false;
    }else{

        QSqlDatabase::database().transaction();
        QSqlQuery query(this->dbConnect);

        if(s->id == NULL){
            query.prepare("INSERT OR REPLACE INTO `Sheets`(`DOZENT_ID`,`TITLE`,`DESCRIPTION`,`EDITOR`,`SUPERVISOR`,`SEMESTER`,`COMPANY`,`TIME_START`,`TIME_END`,`STATUS`,`SUBJECT`,`MAINSUBJECT`,`TYPE`) VALUES (:did,:title,:description,:editor,:supervisor,:semester,:company,:start,:end,:status,:subject,:mainsubject,:type);");
        }else{
            query.prepare("INSERT OR REPLACE INTO `Sheets`(`ID`,`DOZENT_ID`,`TITLE`,`DESCRIPTION`,`EDITOR`,`SUPERVISOR`,`SEMESTER`,`COMPANY`,`TIME_START`,`TIME_END`,`STATUS`,`SUBJECT`,`MAINSUBJECT`,`TYPE`) VALUES (:id,:did,:title,:description,:editor,:supervisor,:semester,:company,:start,:end,:status,:subject,:mainsubject,:type);");
            query.bindValue(":id", s->id);
        }

        query.bindValue(":did", s->dozent_id);
        query.bindValue(":title", s->title);
        query.bindValue(":description", s->description);
        query.bindValue(":editor", s->editor);
        query.bindValue(":supervisor", s->supervisor);
        query.bindValue(":semester", s->semester);
        query.bindValue(":company", s->company);
        query.bindValue(":start", s->start);
        query.bindValue(":end", s->end);
        query.bindValue(":status", s->status);
        query.bindValue(":mainsubject", s->mainSubject);
        query.bindValue(":type", s->sheet);
        query.bindValue(":subject", s->subject);
        if(query.exec()){

            if(s->id == NULL)
                s->id = query.lastInsertId().toInt();

            query.prepare("DELETE FROM Keywords WHERE SHEET_ID = :id");
            query.bindValue(":id", s->id);
            if(!query.exec()){
                transaction_success = false;
            }

            if(transaction_success)
            foreach (const QString &str, s->keywords) {
                   if(str.size()>0){
                       query.prepare("INSERT INTO `Keywords`(`SHEET_ID`,`KEYWORD`) VALUES (:id,:keyword)");
                       query.bindValue(":id", s->id);
                       query.bindValue(":keyword", str);
                       if(!query.exec()){
                           transaction_success = false;
                           break;
                       }
                   }
            }




        }else{
            transaction_success = false;
            QMessageBox::StandardButton reply;
              reply = QMessageBox::information(NULL,"Info",query.lastError().text(),QMessageBox::Yes);
        }

    }

    if(transaction_success){
        QSqlDatabase::database().commit();
        return true;
    }else{
         QSqlDatabase::database().rollback();
         return false;
    }

}

/**
 *  \brief Löscht Dokument aus der Datenbank.
 *  \param s Dokument Object .
 *
 *   Löscht Dokument aus der Datenbank.
 *   \return bool
 */
bool DatabaseService::deleteSheet(Sheet *s) {

    if(!this->dbConnect.isOpen()){
        return false;

    }else{

        QSqlQuery query(this->dbConnect);
        query.prepare("DELETE FROM Sheets WHERE ID =:id");
        query.bindValue(":id", s->id);
        if(query.exec()){

            QMessageBox::StandardButton reply;
              reply = QMessageBox::information(NULL,"Info","Dokument erfolgreich gelöscht",QMessageBox::Yes);
            return true;
        }
    }
    QMessageBox::StandardButton reply;
    reply = QMessageBox::warning(NULL,"Warnung","Fehler beim löschen aufgetreten",QMessageBox::Yes);
    return false;
}

/**
 *  \brief Laden von Dokumenten aus der Datenbank.
 *  \param keywords Stichwörter für die Suche nach Dokumenten .
 *
 *   Sucht Dokumente in der Datenbank, welche angegebene Stichwörter beinhalten. Sind keine Stichwörter vorhanden, so werden alle Dokumente geladen.
 *   \return std::vector<ListItem*>
 */
std::vector<ListItem*> DatabaseService::loadSheets(QStringList keywords) {

    std::vector<ListItem*> list;


    if(!this->dbConnect.isOpen()){
        return list;
    }else{

        QSqlQuery query(this->dbConnect);
        QSqlQuery query2(this->dbConnect);
        QString keywordstring;
        int c = keywords.count();
        if(c>0){
            keywordstring = '"'+keywords.join("\",\"")+'"';
            query.prepare("SELECT ID,TITLE,DESCRIPTION,EDITOR,SUPERVISOR,SEMESTER,COMPANY,TIME_START,TIME_END,STATUS,SUBJECT,MAINSUBJECT,TYPE,DOZENT_ID FROM Sheets WHERE ID IN (SELECT SHEET_ID FROM Keywords WHERE KEYWORD IN ("+keywordstring+"))");
        }else{
             query.prepare("SELECT ID,TITLE,DESCRIPTION,EDITOR,SUPERVISOR,SEMESTER,COMPANY,TIME_START,TIME_END,STATUS,SUBJECT,MAINSUBJECT,TYPE,DOZENT_ID FROM Sheets");
        }

        if(query.exec()){

            while(query.next()){

                int did = query.value(9).toInt();
                Sheet* s = new Sheet(did);

                s->id = query.value(0).toInt();

                s->title = query.value(1).toString();
                s->description= query.value(2).toString();
                s->editor = query.value(3).toString();
                s->supervisor = query.value(4).toString();
                s->semester = query.value(5).toString();
                s->company =query.value(6).toString();
                s->start = query.value(7).toString();
                s->end = query.value(8).toString();

                s->sheet = static_cast<SheetType::EnumSheetType>(query.value(12).toInt());
                s->status = static_cast<ProjectStatus::EnumProjectStatus>(query.value(9).toInt());
                s->subject = static_cast<Subject::EnumSubject>(query.value(10).toInt());
                s->mainSubject = static_cast<MainSubject::EnumMainSubject>(query.value(11).toInt());

                QStringList keywords;
                query2.prepare("SELECT KEYWORD FROM Keywords WHERE SHEET_ID =:id");
                query2.bindValue(":id", s->id);
                if(query2.exec()){

                    while(query2.next()){
                        keywords.append(query2.value(0).toString());
                    }
                     s->keywords =keywords;
                }

                list.push_back(s);
            }

            return list;


        }
    }

    return list;

}

/**
 *  \brief Laden von \link User \endlink aus der Datenbank.
 *
 *   Ladet alle \link User \endlink aus der Datenbank.
 *   \return std::vector<ListItem*>
 */
std::vector<ListItem*> DatabaseService::loadUsers() {

    std::vector<ListItem*> list;


    if(!this->dbConnect.isOpen()){
        return list;
    }else{

        QSqlQuery query(this->dbConnect);
        query.prepare("SELECT EMAIL,NAME,ACCOUNTTYPE FROM User WHERE ACCOUNTTYPE = :accounttype");
        query.bindValue(":accounttype", UserType::DOZENT);

        if(query.exec()){

            while(query.next()){

                User* u = new User();

                u->email = query.value(0).toString();
                u->name = query.value(1).toString();
                u->usertype = static_cast<UserType::EnumUsertype>(query.value(2).toInt());
                list.push_back(u);
            }

            return list;


        }
    }

    return list;

}

