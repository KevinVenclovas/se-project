/********************************************************************************
** Form generated from reading UI file 'login.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGIN_H
#define UI_LOGIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Login
{
public:
    QWidget *centralWidget;
    QPushButton *btn_login;
    QPushButton *btn_login_guest;
    QPushButton *btn_exit;
    QLineEdit *ip_email;
    QLineEdit *ip_password;
    QLabel *label;
    QLabel *label_2;

    void setupUi(QMainWindow *Login)
    {
        if (Login->objectName().isEmpty())
            Login->setObjectName(QStringLiteral("Login"));
        Login->resize(521, 382);
        centralWidget = new QWidget(Login);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        btn_login = new QPushButton(centralWidget);
        btn_login->setObjectName(QStringLiteral("btn_login"));
        btn_login->setGeometry(QRect(150, 190, 221, 41));
        QFont font;
        font.setPointSize(12);
        btn_login->setFont(font);
        btn_login_guest = new QPushButton(centralWidget);
        btn_login_guest->setObjectName(QStringLiteral("btn_login_guest"));
        btn_login_guest->setGeometry(QRect(150, 240, 221, 41));
        btn_login_guest->setFont(font);
        btn_exit = new QPushButton(centralWidget);
        btn_exit->setObjectName(QStringLiteral("btn_exit"));
        btn_exit->setGeometry(QRect(150, 290, 221, 41));
        btn_exit->setFont(font);
        ip_email = new QLineEdit(centralWidget);
        ip_email->setObjectName(QStringLiteral("ip_email"));
        ip_email->setGeometry(QRect(240, 50, 241, 20));
        ip_password = new QLineEdit(centralWidget);
        ip_password->setObjectName(QStringLiteral("ip_password"));
        ip_password->setGeometry(QRect(240, 100, 241, 20));
        ip_password->setEchoMode(QLineEdit::Password);
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(50, 50, 161, 16));
        label->setFont(font);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(50, 100, 171, 16));
        label_2->setFont(font);
        Login->setCentralWidget(centralWidget);

        retranslateUi(Login);

        QMetaObject::connectSlotsByName(Login);
    } // setupUi

    void retranslateUi(QMainWindow *Login)
    {
        Login->setWindowTitle(QApplication::translate("Login", "Login", 0));
        btn_login->setText(QApplication::translate("Login", "Anmelden", 0));
        btn_login_guest->setText(QApplication::translate("Login", "Anmelden als Gast", 0));
        btn_exit->setText(QApplication::translate("Login", "Beenden", 0));
        label->setText(QApplication::translate("Login", "E-Mail", 0));
        label_2->setText(QApplication::translate("Login", "Passwort", 0));
    } // retranslateUi

};

namespace Ui {
    class Login: public Ui_Login {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGIN_H
