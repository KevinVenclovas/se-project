/********************************************************************************
** Form generated from reading UI file 'createdozent.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREATEDOZENT_H
#define UI_CREATEDOZENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CreateDozent
{
public:
    QWidget *centralwidget;
    QPushButton *btn_ok;
    QPushButton *btn_exit;
    QLineEdit *ip_email;
    QLineEdit *ip_name;
    QLabel *label;
    QLabel *label_2;

    void setupUi(QMainWindow *CreateDozent)
    {
        if (CreateDozent->objectName().isEmpty())
            CreateDozent->setObjectName(QStringLiteral("CreateDozent"));
        CreateDozent->resize(472, 316);
        centralwidget = new QWidget(CreateDozent);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        btn_ok = new QPushButton(centralwidget);
        btn_ok->setObjectName(QStringLiteral("btn_ok"));
        btn_ok->setGeometry(QRect(50, 230, 141, 41));
        QFont font;
        font.setPointSize(12);
        btn_ok->setFont(font);
        btn_exit = new QPushButton(centralwidget);
        btn_exit->setObjectName(QStringLiteral("btn_exit"));
        btn_exit->setGeometry(QRect(270, 230, 141, 41));
        btn_exit->setFont(font);
        ip_email = new QLineEdit(centralwidget);
        ip_email->setObjectName(QStringLiteral("ip_email"));
        ip_email->setGeometry(QRect(200, 60, 221, 20));
        ip_name = new QLineEdit(centralwidget);
        ip_name->setObjectName(QStringLiteral("ip_name"));
        ip_name->setGeometry(QRect(200, 120, 221, 20));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(60, 60, 55, 16));
        label->setFont(font);
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(60, 120, 171, 16));
        label_2->setFont(font);
        label_2->setFrameShape(QFrame::NoFrame);
        CreateDozent->setCentralWidget(centralwidget);

        retranslateUi(CreateDozent);

        QMetaObject::connectSlotsByName(CreateDozent);
    } // setupUi

    void retranslateUi(QMainWindow *CreateDozent)
    {
        CreateDozent->setWindowTitle(QApplication::translate("CreateDozent", "MainWindow", 0));
        btn_ok->setText(QApplication::translate("CreateDozent", "OK", 0));
        btn_exit->setText(QApplication::translate("CreateDozent", "Abbrechen", 0));
        label->setText(QApplication::translate("CreateDozent", "E-Mail", 0));
        label_2->setText(QApplication::translate("CreateDozent", "Name", 0));
    } // retranslateUi

};

namespace Ui {
    class CreateDozent: public Ui_CreateDozent {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREATEDOZENT_H
