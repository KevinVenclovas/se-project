/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSearch;
    QAction *actionLogout;
    QWidget *centralwidget;
    QListView *ls_data;
    QWidget *formLayoutWidget;
    QFormLayout *lay_btn;
    QLineEdit *ip_keywords;
    QPushButton *btn_search;
    QPushButton *btn_reset;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(800, 615);
        actionSearch = new QAction(MainWindow);
        actionSearch->setObjectName(QStringLiteral("actionSearch"));
        actionLogout = new QAction(MainWindow);
        actionLogout->setObjectName(QStringLiteral("actionLogout"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        ls_data = new QListView(centralwidget);
        ls_data->setObjectName(QStringLiteral("ls_data"));
        ls_data->setGeometry(QRect(10, 10, 451, 511));
        formLayoutWidget = new QWidget(centralwidget);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(480, 10, 311, 511));
        lay_btn = new QFormLayout(formLayoutWidget);
        lay_btn->setObjectName(QStringLiteral("lay_btn"));
        lay_btn->setContentsMargins(0, 0, 0, 0);
        ip_keywords = new QLineEdit(centralwidget);
        ip_keywords->setObjectName(QStringLiteral("ip_keywords"));
        ip_keywords->setGeometry(QRect(10, 530, 291, 20));
        btn_search = new QPushButton(centralwidget);
        btn_search->setObjectName(QStringLiteral("btn_search"));
        btn_search->setGeometry(QRect(310, 530, 75, 23));
        btn_reset = new QPushButton(centralwidget);
        btn_reset->setObjectName(QStringLiteral("btn_reset"));
        btn_reset->setGeometry(QRect(390, 530, 75, 23));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 26));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionSearch->setText(QApplication::translate("MainWindow", "Dokument Suchen", 0));
        actionLogout->setText(QApplication::translate("MainWindow", "Logout", 0));
        ip_keywords->setPlaceholderText(QApplication::translate("MainWindow", "Beispiel: Word,Key,Arbeit", 0));
        btn_search->setText(QApplication::translate("MainWindow", "Suchen", 0));
        btn_reset->setText(QApplication::translate("MainWindow", "Reset", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
