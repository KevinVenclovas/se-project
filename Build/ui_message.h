/********************************************************************************
** Form generated from reading UI file 'message.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MESSAGE_H
#define UI_MESSAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Message
{
public:
    QWidget *centralwidget;
    QLabel *lb_message;
    QPushButton *btn_ok;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *Message)
    {
        if (Message->objectName().isEmpty())
            Message->setObjectName(QStringLiteral("Message"));
        Message->resize(800, 600);
        centralwidget = new QWidget(Message);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        lb_message = new QLabel(centralwidget);
        lb_message->setObjectName(QStringLiteral("lb_message"));
        lb_message->setGeometry(QRect(60, 60, 561, 101));
        lb_message->setAlignment(Qt::AlignCenter);
        lb_message->setWordWrap(true);
        btn_ok = new QPushButton(centralwidget);
        btn_ok->setObjectName(QStringLiteral("btn_ok"));
        btn_ok->setGeometry(QRect(280, 250, 381, 151));
        Message->setCentralWidget(centralwidget);
        menubar = new QMenuBar(Message);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 21));
        Message->setMenuBar(menubar);
        statusbar = new QStatusBar(Message);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        Message->setStatusBar(statusbar);

        retranslateUi(Message);

        QMetaObject::connectSlotsByName(Message);
    } // setupUi

    void retranslateUi(QMainWindow *Message)
    {
        Message->setWindowTitle(QApplication::translate("Message", "MainWindow", 0));
        lb_message->setText(QApplication::translate("Message", "TextLabel", 0));
        btn_ok->setText(QApplication::translate("Message", "PushButton", 0));
    } // retranslateUi

};

namespace Ui {
    class Message: public Ui_Message {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MESSAGE_H
