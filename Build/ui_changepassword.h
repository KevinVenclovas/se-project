/********************************************************************************
** Form generated from reading UI file 'changepassword.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHANGEPASSWORD_H
#define UI_CHANGEPASSWORD_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ChangePassword
{
public:
    QWidget *centralwidget;
    QLineEdit *ip_currentpw;
    QLineEdit *ip_newpw;
    QLineEdit *ip_newpw2;
    QPushButton *btn_ok;
    QPushButton *btn_exit;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;

    void setupUi(QMainWindow *ChangePassword)
    {
        if (ChangePassword->objectName().isEmpty())
            ChangePassword->setObjectName(QStringLiteral("ChangePassword"));
        ChangePassword->resize(530, 321);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ChangePassword->sizePolicy().hasHeightForWidth());
        ChangePassword->setSizePolicy(sizePolicy);
        centralwidget = new QWidget(ChangePassword);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        ip_currentpw = new QLineEdit(centralwidget);
        ip_currentpw->setObjectName(QStringLiteral("ip_currentpw"));
        ip_currentpw->setGeometry(QRect(220, 50, 251, 20));
        ip_newpw = new QLineEdit(centralwidget);
        ip_newpw->setObjectName(QStringLiteral("ip_newpw"));
        ip_newpw->setGeometry(QRect(220, 100, 251, 20));
        ip_newpw2 = new QLineEdit(centralwidget);
        ip_newpw2->setObjectName(QStringLiteral("ip_newpw2"));
        ip_newpw2->setGeometry(QRect(220, 150, 251, 20));
        btn_ok = new QPushButton(centralwidget);
        btn_ok->setObjectName(QStringLiteral("btn_ok"));
        btn_ok->setGeometry(QRect(50, 230, 141, 41));
        QFont font;
        font.setPointSize(12);
        btn_ok->setFont(font);
        btn_exit = new QPushButton(centralwidget);
        btn_exit->setObjectName(QStringLiteral("btn_exit"));
        btn_exit->setGeometry(QRect(330, 230, 141, 41));
        btn_exit->setFont(font);
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 50, 161, 16));
        label->setFont(font);
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(40, 100, 171, 16));
        label_2->setFont(font);
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(40, 150, 161, 16));
        label_3->setFont(font);
        ChangePassword->setCentralWidget(centralwidget);

        retranslateUi(ChangePassword);

        QMetaObject::connectSlotsByName(ChangePassword);
    } // setupUi

    void retranslateUi(QMainWindow *ChangePassword)
    {
        ChangePassword->setWindowTitle(QApplication::translate("ChangePassword", "MainWindow", 0));
        btn_ok->setText(QApplication::translate("ChangePassword", "OK", 0));
        btn_exit->setText(QApplication::translate("ChangePassword", "Abbrechen", 0));
        label->setText(QApplication::translate("ChangePassword", "Altes Passwort", 0));
        label_2->setText(QApplication::translate("ChangePassword", "Neues Passwort", 0));
        label_3->setText(QApplication::translate("ChangePassword", "Neues Passwort", 0));
    } // retranslateUi

};

namespace Ui {
    class ChangePassword: public Ui_ChangePassword {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHANGEPASSWORD_H
