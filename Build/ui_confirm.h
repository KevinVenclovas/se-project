/********************************************************************************
** Form generated from reading UI file 'confirm.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONFIRM_H
#define UI_CONFIRM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Confirm
{
public:
    QWidget *centralwidget;
    QPushButton *btn_confirm;
    QPushButton *btn_exit;
    QLabel *lb_message;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *Confirm)
    {
        if (Confirm->objectName().isEmpty())
            Confirm->setObjectName(QStringLiteral("Confirm"));
        Confirm->resize(800, 600);
        centralwidget = new QWidget(Confirm);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        btn_confirm = new QPushButton(centralwidget);
        btn_confirm->setObjectName(QStringLiteral("btn_confirm"));
        btn_confirm->setGeometry(QRect(124, 362, 231, 131));
        btn_exit = new QPushButton(centralwidget);
        btn_exit->setObjectName(QStringLiteral("btn_exit"));
        btn_exit->setGeometry(QRect(520, 452, 121, 41));
        lb_message = new QLabel(centralwidget);
        lb_message->setObjectName(QStringLiteral("lb_message"));
        lb_message->setGeometry(QRect(140, 80, 471, 141));
        lb_message->setAlignment(Qt::AlignCenter);
        lb_message->setWordWrap(true);
        Confirm->setCentralWidget(centralwidget);
        menubar = new QMenuBar(Confirm);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 21));
        Confirm->setMenuBar(menubar);
        statusbar = new QStatusBar(Confirm);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        Confirm->setStatusBar(statusbar);

        retranslateUi(Confirm);

        QMetaObject::connectSlotsByName(Confirm);
    } // setupUi

    void retranslateUi(QMainWindow *Confirm)
    {
        Confirm->setWindowTitle(QApplication::translate("Confirm", "MainWindow", 0));
        btn_confirm->setText(QApplication::translate("Confirm", "PushButton", 0));
        btn_exit->setText(QApplication::translate("Confirm", "PushButton", 0));
        lb_message->setText(QApplication::translate("Confirm", "TextLabel", 0));
    } // retranslateUi

};

namespace Ui {
    class Confirm: public Ui_Confirm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONFIRM_H
