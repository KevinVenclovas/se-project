/********************************************************************************
** Form generated from reading UI file 'selectsheettype.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTSHEETTYPE_H
#define UI_SELECTSHEETTYPE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SelectSheetType
{
public:
    QWidget *centralwidget;
    QPushButton *btn_arbeit;
    QPushButton *btn_Project;
    QPushButton *btn_abschlussarbeit;
    QPushButton *btn_exit;

    void setupUi(QMainWindow *SelectSheetType)
    {
        if (SelectSheetType->objectName().isEmpty())
            SelectSheetType->setObjectName(QStringLiteral("SelectSheetType"));
        SelectSheetType->resize(420, 274);
        centralwidget = new QWidget(SelectSheetType);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        btn_arbeit = new QPushButton(centralwidget);
        btn_arbeit->setObjectName(QStringLiteral("btn_arbeit"));
        btn_arbeit->setGeometry(QRect(100, 40, 211, 31));
        QFont font;
        font.setPointSize(12);
        btn_arbeit->setFont(font);
        btn_Project = new QPushButton(centralwidget);
        btn_Project->setObjectName(QStringLiteral("btn_Project"));
        btn_Project->setGeometry(QRect(100, 90, 211, 31));
        btn_Project->setFont(font);
        btn_abschlussarbeit = new QPushButton(centralwidget);
        btn_abschlussarbeit->setObjectName(QStringLiteral("btn_abschlussarbeit"));
        btn_abschlussarbeit->setGeometry(QRect(100, 140, 211, 31));
        btn_abschlussarbeit->setFont(font);
        btn_exit = new QPushButton(centralwidget);
        btn_exit->setObjectName(QStringLiteral("btn_exit"));
        btn_exit->setGeometry(QRect(100, 190, 211, 31));
        btn_exit->setFont(font);
        SelectSheetType->setCentralWidget(centralwidget);

        retranslateUi(SelectSheetType);

        QMetaObject::connectSlotsByName(SelectSheetType);
    } // setupUi

    void retranslateUi(QMainWindow *SelectSheetType)
    {
        SelectSheetType->setWindowTitle(QApplication::translate("SelectSheetType", "MainWindow", 0));
        btn_arbeit->setText(QApplication::translate("SelectSheetType", "Arbeit", 0));
        btn_Project->setText(QApplication::translate("SelectSheetType", "Projektarbeit", 0));
        btn_abschlussarbeit->setText(QApplication::translate("SelectSheetType", "Abschlussarbeit", 0));
        btn_exit->setText(QApplication::translate("SelectSheetType", "Abbrechen", 0));
    } // retranslateUi

};

namespace Ui {
    class SelectSheetType: public Ui_SelectSheetType {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTSHEETTYPE_H
