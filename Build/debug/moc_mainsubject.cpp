/****************************************************************************
** Meta object code from reading C++ file 'mainsubject.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../mainsubject.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainsubject.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainSubject_t {
    QByteArrayData data[7];
    char stringdata[87];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainSubject_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainSubject_t qt_meta_stringdata_MainSubject = {
    {
QT_MOC_LITERAL(0, 0, 11), // "MainSubject"
QT_MOC_LITERAL(1, 12, 15), // "EnumMainSubject"
QT_MOC_LITERAL(2, 28, 10), // "IN_PROJECT"
QT_MOC_LITERAL(3, 39, 10), // "IS_PROJECT"
QT_MOC_LITERAL(4, 50, 10), // "MI_PROJECT"
QT_MOC_LITERAL(5, 61, 10), // "SE_PROJECT"
QT_MOC_LITERAL(6, 72, 14) // "MASTER_PROJECT"

    },
    "MainSubject\0EnumMainSubject\0IN_PROJECT\0"
    "IS_PROJECT\0MI_PROJECT\0SE_PROJECT\0"
    "MASTER_PROJECT"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainSubject[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, flags, count, data
       1, 0x0,    5,   18,

 // enum data: key, value
       2, uint(MainSubject::IN_PROJECT),
       3, uint(MainSubject::IS_PROJECT),
       4, uint(MainSubject::MI_PROJECT),
       5, uint(MainSubject::SE_PROJECT),
       6, uint(MainSubject::MASTER_PROJECT),

       0        // eod
};

void MainSubject::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject MainSubject::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_MainSubject.data,
      qt_meta_data_MainSubject,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainSubject::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainSubject::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainSubject.stringdata))
        return static_cast<void*>(const_cast< MainSubject*>(this));
    return QObject::qt_metacast(_clname);
}

int MainSubject::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
