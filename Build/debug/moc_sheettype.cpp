/****************************************************************************
** Meta object code from reading C++ file 'sheettype.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../sheettype.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'sheettype.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_SheetType_t {
    QByteArrayData data[5];
    char stringdata[61];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SheetType_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SheetType_t qt_meta_stringdata_SheetType = {
    {
QT_MOC_LITERAL(0, 0, 9), // "SheetType"
QT_MOC_LITERAL(1, 10, 13), // "EnumSheetType"
QT_MOC_LITERAL(2, 24, 6), // "ARBEIT"
QT_MOC_LITERAL(3, 31, 13), // "PROJECTARBEIT"
QT_MOC_LITERAL(4, 45, 15) // "ABSCHLUSSARBEIT"

    },
    "SheetType\0EnumSheetType\0ARBEIT\0"
    "PROJECTARBEIT\0ABSCHLUSSARBEIT"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SheetType[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, flags, count, data
       1, 0x0,    3,   18,

 // enum data: key, value
       2, uint(SheetType::ARBEIT),
       3, uint(SheetType::PROJECTARBEIT),
       4, uint(SheetType::ABSCHLUSSARBEIT),

       0        // eod
};

void SheetType::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject SheetType::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_SheetType.data,
      qt_meta_data_SheetType,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *SheetType::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SheetType::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_SheetType.stringdata))
        return static_cast<void*>(const_cast< SheetType*>(this));
    return QObject::qt_metacast(_clname);
}

int SheetType::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
