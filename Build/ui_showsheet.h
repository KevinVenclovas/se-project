/********************************************************************************
** Form generated from reading UI file 'showsheet.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SHOWSHEET_H
#define UI_SHOWSHEET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ShowSheet
{
public:
    QWidget *centralwidget;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *lay_sheet_btn;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLineEdit *txt_title;
    QLineEdit *txt_editor;
    QLineEdit *txt_supervisor;
    QLineEdit *txt_keywords;
    QTextEdit *txt_description;
    QComboBox *cmb_status;
    QComboBox *cmb_subject;
    QComboBox *cmb_main_subject;
    QLineEdit *txt_company;
    QLineEdit *txt_semester;
    QLabel *label_11;
    QDateEdit *date_start;
    QLabel *label_12;
    QDateEdit *date_end;

    void setupUi(QMainWindow *ShowSheet)
    {
        if (ShowSheet->objectName().isEmpty())
            ShowSheet->setObjectName(QStringLiteral("ShowSheet"));
        ShowSheet->resize(800, 816);
        centralwidget = new QWidget(ShowSheet);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(50, 40, 55, 16));
        QFont font;
        font.setPointSize(12);
        label->setFont(font);
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(50, 80, 131, 31));
        label_2->setFont(font);
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(50, 200, 121, 16));
        label_3->setFont(font);
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(50, 250, 101, 16));
        label_4->setFont(font);
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(50, 300, 111, 16));
        label_5->setFont(font);
        label_6 = new QLabel(centralwidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(50, 350, 111, 16));
        label_6->setFont(font);
        horizontalLayoutWidget = new QWidget(centralwidget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(30, 720, 741, 80));
        lay_sheet_btn = new QHBoxLayout(horizontalLayoutWidget);
        lay_sheet_btn->setObjectName(QStringLiteral("lay_sheet_btn"));
        lay_sheet_btn->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(centralwidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(50, 390, 121, 41));
        label_7->setFont(font);
        label_8 = new QLabel(centralwidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(50, 490, 131, 31));
        label_8->setFont(font);
        label_9 = new QLabel(centralwidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(50, 550, 161, 16));
        label_9->setFont(font);
        label_10 = new QLabel(centralwidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(50, 630, 111, 16));
        label_10->setFont(font);
        txt_title = new QLineEdit(centralwidget);
        txt_title->setObjectName(QStringLiteral("txt_title"));
        txt_title->setGeometry(QRect(230, 40, 511, 22));
        txt_editor = new QLineEdit(centralwidget);
        txt_editor->setObjectName(QStringLiteral("txt_editor"));
        txt_editor->setGeometry(QRect(230, 300, 511, 22));
        txt_supervisor = new QLineEdit(centralwidget);
        txt_supervisor->setObjectName(QStringLiteral("txt_supervisor"));
        txt_supervisor->setGeometry(QRect(230, 350, 511, 22));
        txt_keywords = new QLineEdit(centralwidget);
        txt_keywords->setObjectName(QStringLiteral("txt_keywords"));
        txt_keywords->setGeometry(QRect(230, 200, 511, 22));
        txt_description = new QTextEdit(centralwidget);
        txt_description->setObjectName(QStringLiteral("txt_description"));
        txt_description->setGeometry(QRect(230, 90, 511, 87));
        cmb_status = new QComboBox(centralwidget);
        cmb_status->setObjectName(QStringLiteral("cmb_status"));
        cmb_status->setGeometry(QRect(230, 250, 511, 22));
        cmb_subject = new QComboBox(centralwidget);
        cmb_subject->setObjectName(QStringLiteral("cmb_subject"));
        cmb_subject->setGeometry(QRect(230, 500, 511, 22));
        cmb_main_subject = new QComboBox(centralwidget);
        cmb_main_subject->setObjectName(QStringLiteral("cmb_main_subject"));
        cmb_main_subject->setGeometry(QRect(230, 400, 511, 22));
        txt_company = new QLineEdit(centralwidget);
        txt_company->setObjectName(QStringLiteral("txt_company"));
        txt_company->setGeometry(QRect(230, 630, 511, 22));
        txt_semester = new QLineEdit(centralwidget);
        txt_semester->setObjectName(QStringLiteral("txt_semester"));
        txt_semester->setGeometry(QRect(230, 450, 511, 22));
        label_11 = new QLabel(centralwidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(50, 450, 141, 16));
        label_11->setFont(font);
        date_start = new QDateEdit(centralwidget);
        date_start->setObjectName(QStringLiteral("date_start"));
        date_start->setGeometry(QRect(230, 550, 110, 22));
        label_12 = new QLabel(centralwidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(50, 590, 161, 16));
        label_12->setFont(font);
        date_end = new QDateEdit(centralwidget);
        date_end->setObjectName(QStringLiteral("date_end"));
        date_end->setGeometry(QRect(230, 590, 110, 22));
        ShowSheet->setCentralWidget(centralwidget);

        retranslateUi(ShowSheet);

        QMetaObject::connectSlotsByName(ShowSheet);
    } // setupUi

    void retranslateUi(QMainWindow *ShowSheet)
    {
        ShowSheet->setWindowTitle(QApplication::translate("ShowSheet", "MainWindow", 0));
        label->setText(QApplication::translate("ShowSheet", "Titel", 0));
        label_2->setText(QApplication::translate("ShowSheet", "Beschreibung", 0));
        label_3->setText(QApplication::translate("ShowSheet", "Stichw\303\266rter", 0));
        label_4->setText(QApplication::translate("ShowSheet", "Status", 0));
        label_5->setText(QApplication::translate("ShowSheet", "Bearbeiter", 0));
        label_6->setText(QApplication::translate("ShowSheet", "Betreuer", 0));
        label_7->setText(QApplication::translate("ShowSheet", "Projekt", 0));
        label_8->setText(QApplication::translate("ShowSheet", "Schwerpunkt", 0));
        label_9->setText(QApplication::translate("ShowSheet", "Zeitraum - Start", 0));
        label_10->setText(QApplication::translate("ShowSheet", "Firma", 0));
        txt_keywords->setPlaceholderText(QApplication::translate("ShowSheet", "Beispiel: Word,Key,Arbeit", 0));
        cmb_status->clear();
        cmb_status->insertItems(0, QStringList()
         << QApplication::translate("ShowSheet", "In Arbeit", 0)
         << QApplication::translate("ShowSheet", "Abgeschlossen", 0)
        );
        cmb_subject->clear();
        cmb_subject->insertItems(0, QStringList()
         << QApplication::translate("ShowSheet", "IN", 0)
         << QApplication::translate("ShowSheet", "IS", 0)
         << QApplication::translate("ShowSheet", "MI", 0)
         << QApplication::translate("ShowSheet", "SE", 0)
        );
        cmb_main_subject->clear();
        cmb_main_subject->insertItems(0, QStringList()
         << QApplication::translate("ShowSheet", "IN-Projekt", 0)
         << QApplication::translate("ShowSheet", "IS-Projekt", 0)
         << QApplication::translate("ShowSheet", "MI-Projekt", 0)
         << QApplication::translate("ShowSheet", "SE-Projekt", 0)
         << QApplication::translate("ShowSheet", "Master-Projekt", 0)
        );
        label_11->setText(QApplication::translate("ShowSheet", "Semester", 0));
        date_start->setDisplayFormat(QApplication::translate("ShowSheet", "dd-MM-yyyy", 0));
        label_12->setText(QApplication::translate("ShowSheet", "Zeitraum - End", 0));
        date_end->setDisplayFormat(QApplication::translate("ShowSheet", "dd-MM-yyyy", 0));
    } // retranslateUi

};

namespace Ui {
    class ShowSheet: public Ui_ShowSheet {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SHOWSHEET_H
