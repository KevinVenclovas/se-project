#-------------------------------------------------
#
# Project created by QtCreator 2018-01-09T18:26:38
#
#-------------------------------------------------

QT       += core gui sql


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Se-Project
TEMPLATE = app
CONFIG += C++14

SOURCES += main.cpp\
        login.cpp \
    user.cpp \
    mainwindow.cpp \
    passwordservice.cpp \
    changepassword.cpp \
    createdozent.cpp \
    databaseservice.cpp \
    sheet.cpp \
    showsheet.cpp \
    selectsheettype.cpp \
    listitem.cpp

HEADERS  += login.h \
    user.h \
    mainwindow.h \
    passwordservice.h \
    changepassword.h \
    createdozent.h \
    databaseservice.h \
    usertype.h \
    mainsubject.h \
    sheettype.h \
    projectstatus.h \
    subject.h \
    sheet.h \
    showsheet.h \
    selectsheettype.h \
    listitem.h

FORMS    += login.ui \
    mainwindow.ui \
    changepassword.ui \
    createdozent.ui \
    showsheet.ui \
    selectsheettype.ui
