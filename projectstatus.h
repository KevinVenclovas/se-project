/** \file
 *  Enumeration für alle verfügbaren Dokumentstatuse.
 */
#ifndef PROJECTSTATUS_H
#define PROJECTSTATUS_H

#include <QObject>
#include <QMetaEnum>
class ProjectStatus : public QObject
{
    Q_OBJECT
    Q_ENUMS(EnumProjectStatus)
public:

public:
    explicit ProjectStatus (QObject *parent = 0);

    enum EnumProjectStatus{
        INWORK,
        FINISH
    };


};

#endif // PROJECTSTATUS_H
