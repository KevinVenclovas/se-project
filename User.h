/** \file
 * User Object für das Verwalten von Admin und Dozenten.
 */
#ifndef USER_H
#define USER_H
#include "QString.h"
#include "UserType.h"
#include "ListItem.h"
class User : public ListItem
{
public:
    User(int id,QString email, QString name, UserType::EnumUsertype usertype);
    User();
    int id;
    QString email;
    QString name;
    UserType::EnumUsertype usertype;

protected:
    virtual QString zeigeName()
    {
        return name;
    }
};

#endif // USER_H
