/** \file
 *  UI für das erstellen eines Dozenten
 */
#ifndef CREATEDOZENT_H
#define CREATEDOZENT_H

#include <QMainWindow>
#include "MainWindow.h"
namespace Ui {
class CreateDozent;
}

class CreateDozent : public QMainWindow
{
    Q_OBJECT

public:
    explicit CreateDozent(QWidget *parent = 0);
    ~CreateDozent();    
    void setUpAndShow(MainWindow* parent);
    void createDozent();
    MainWindow* parent;

private slots:
    void on_btn_ok_clicked();
    void on_btn_exit_clicked();

private:
    Ui::CreateDozent *ui;
};

#endif // CREATEDOZENT_H
