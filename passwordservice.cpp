#include "passwordservice.h"

PasswordService::PasswordService() {
}

/**
 *  \brief Überprüft ob Passwörter gleich sind.
 *  \param passwort1 Erstes Passwort
 *  \param passwort2 Wiederholung des Passwortes
 */
// check ob das neu eingegebene Passwort übereinstimmt
bool PasswordService::checkNewPassword(QString password1, QString password2) {
    if (password1.compare(password2) == 0) {
        return true;
    }
    return false;
}

/**
 *  \brief Erzeugt Random passwort.
 *  \param length Länge des passwortes
 */
QString PasswordService::GetRandomPassword(int length) const
{
   const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
   const int randomStringLength = length; // assuming you want random strings of 12 characters

   QString randomString;
   for(int i=0; i<randomStringLength; ++i)
   {
       int index = qrand() % possibleCharacters.length();
       QChar nextChar = possibleCharacters.at(index);
       randomString.append(nextChar);
   }
   return randomString;
}
