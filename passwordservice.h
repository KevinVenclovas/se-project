#ifndef PASSWORDSERVICE_H
#define PASSWORDSERVICE_H
#include "QString.h";

class PasswordService
{
public:
    PasswordService();
    bool checkNewPassword(QString password1, QString password2);
    QString GetRandomPassword(int lenght) const;
};

#endif // PASSWORDSERVICES_H
