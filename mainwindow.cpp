#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QPushButton.h"
#include "QString.h"
#include "UserType.h"
#include "CreateDozent.h"
#include "Sheet.h"
#include "SelectSheetType.h"
#include "DatabaseService.h"
#include "ListItem.h"
#include "ShowSheet.h"
#include "QMessageBox.h";
#include "changepassword.h";
#include "PasswordService.h";
#include "Login.h";
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 *  \brief Vorbereitung des Hauptfensters.
 *  \param u \link User \endlink der sich eingelogt hat
 *
 */
void MainWindow::setUpAndShow(User* user) {
    this->setWindowTitle("Willkommen " + user->name);
    this->currentuser = user;
    switch (user->usertype) {
        case UserType::ADMIN:
            this->drawAdminView();
            break;
        case UserType::DOZENT:
            this->drawDozentView();
            break;
        case UserType::GUEST:
            this->drawGuestView();
            break;
        default:
            break;
    }

    QAction *logout = ui->menubar->addAction("Logout");
    connect(logout, SIGNAL(triggered()), this, SLOT(logout()));


    this->show();
}



/**
 *  \brief Erzeugt Hauptfenster für ein Dozent
 *  Erzeugt Buttons und setzt alle Dokumente aus der Datenbank diese in die Listview
 *
 */
void MainWindow::drawDozentView() {
    //Generieren der Buttons
    QPushButton *createSheet = new QPushButton("Eintrag erstellen");
    QPushButton *deleteSheet = new QPushButton("Eintrag löschen");
    QPushButton *changeSheet = new QPushButton("Eintrag bearbeiten");
    QPushButton *changePasswordDozent = new QPushButton("Passwort ändern");
    QPushButton *openSheet = new QPushButton("Eintrag öffnen");

    // Einfügen der Buttons in das Layout
    ui->lay_btn->addWidget(createSheet);
    ui->lay_btn->addWidget(deleteSheet);
    ui->lay_btn->addWidget(changeSheet);
    ui->lay_btn->addWidget(changePasswordDozent);
    ui->lay_btn->addWidget(openSheet);

    // Connecten der Buttons mit den Funktionen
    QObject::connect(createSheet, SIGNAL(clicked()), this, SLOT(createSheet()));
    QObject::connect(deleteSheet, SIGNAL(clicked()), this, SLOT(deleteSheet()));
    QObject::connect(changeSheet, SIGNAL(clicked()), this, SLOT(changeSheet()));
    QObject::connect(changePasswordDozent, SIGNAL(clicked()), this, SLOT(changePasswordDozent()));
    QObject::connect(openSheet, SIGNAL(clicked()), this, SLOT(openSheet()));

    DatabaseService* db = new DatabaseService();
    QStringList keys;
    std::vector<ListItem*> datalist = db->loadSheets(keys);
    this->listOfItems = datalist;
    this->updateListView(NULL);
}

/**
 *  \brief Erzeugt Admin für ein Dozent
 *  Erzeugt Buttons und setzt alle Dozenten aus der Datenbank diese in die Listview
 *
 */
void MainWindow::drawAdminView() {


    ui->ip_keywords->setVisible(false);
    ui->btn_search->setVisible(false);
    ui->btn_reset->setVisible(false);

    QPushButton *createDozent = new QPushButton("Dozent anlegen");
    QPushButton *deleteDozent = new QPushButton("Dozent löschen");
    QPushButton *changePasswordAdmin = new QPushButton("Passwort eines Dozenten ändern");

    ui->lay_btn->addWidget(createDozent);
    ui->lay_btn->addWidget(deleteDozent);
    ui->lay_btn->addWidget(changePasswordAdmin);

    QObject::connect(createDozent, SIGNAL(clicked()), this, SLOT(createDozent()));
    QObject::connect(deleteDozent, SIGNAL(clicked()), this, SLOT(deleteDozent()));
    QObject::connect(changePasswordAdmin, SIGNAL(clicked()), this, SLOT(changePasswordAdmin()));

    DatabaseService* db = new DatabaseService();

    std::vector<ListItem*> datalist = db->loadUsers();
    this->listOfItems = datalist;
    this->updateListView(NULL);


}

/**
 *  \brief Erzeugt Hauptfenster für ein Dozent
 *  Erzeugt Buttons und setzt alle Dokumente aus der Datenbank diese in die Listview
 *
 */
void MainWindow::drawGuestView() {
    QPushButton *openSheetGuest = new QPushButton("Eintrag öffnen");
    ui->lay_btn->addWidget(openSheetGuest);
    QObject::connect(openSheetGuest, SIGNAL(clicked()), this, SLOT(openSheet()));
    DatabaseService* db = new DatabaseService();
    QStringList keys;
    std::vector<ListItem*> datalist = db->loadSheets(keys);
    this->listOfItems = datalist;
    this->updateListView(NULL);
}

/**
 *  \brief Öffnet das Fenster um einen Dozenten zu erstellen.
 *
 */
void MainWindow::createDozent(){
    CreateDozent *cd = new CreateDozent(this);
    cd->setUpAndShow(this);
}
/**
 *  \brief Öffnet das Fenster um ein neues DOkument zu erstellen.
 */
void MainWindow::createSheet(){

    SelectSheetType* s = new SelectSheetType();
    s->setUpAndShow(this->currentuser,this);

}

/**
 *  \brief Ladet das Selektierte Dokument aus der Liste und öffnet es für die Bearbeitung.
 */
void MainWindow::changeSheet(){

    int i = ui->ls_data->currentIndex().row();
    if(i>=0){
        ListItem* l =  this->listOfItems[i];

        Sheet* sheet = dynamic_cast<Sheet*>(l);

        if(sheet->dozent_id == this->currentuser->id){
            ShowSheet* s = new ShowSheet();
            s->setUpAndShow(this,true,sheet);
        }else{
            QMessageBox::StandardButton reply;
              reply = QMessageBox::critical(this,"Error","Dokument wurde nicht von Ihnen erstellt.",QMessageBox::Yes);
        }



    }else{
        QMessageBox::StandardButton reply;
          reply = QMessageBox::critical(this,"Error","Bitte wählen Sie ein Dokunent aus der Liste aus.",QMessageBox::Yes);
    }
}
/**
 *  \brief Updatet die Liste mit den bearbeiteten Daten.
 */
void MainWindow::updateListView(ListItem* item){

    if(item)
        if (std::find(this->listOfItems.begin(), this->listOfItems.end(), item) == this->listOfItems.end())
            this->listOfItems.push_back(item);


    model = new QStringListModel();
    QStringList viewlist;

    std::vector<ListItem*>::iterator iter = this->listOfItems.begin();

    while(iter != this->listOfItems.end())
    {
        viewlist.append((*iter)->zeigeName());
        iter++;
    }


    model->setStringList(viewlist);
    ui->ls_data->setModel(model);
    ui->ls_data->setEditTriggers(QAbstractItemView::NoEditTriggers);
}
/**
 *  \brief Öffnet ein selektiertes Dokument ohne es bearbeiten zu können.
 */
void MainWindow::openSheet(){

    int i = ui->ls_data->currentIndex().row();

    if(i>=0){
        ListItem* l =  this->listOfItems[i];

        Sheet* sheet = dynamic_cast<Sheet*>(l);
        ShowSheet* s = new ShowSheet();
        s->setUpAndShow(this,false,sheet);
    }else{
        QMessageBox::StandardButton reply;
          reply = QMessageBox::critical(this,"Error","Bitte wählen Sie ein Dokunent aus der Liste aus.",QMessageBox::Yes);
    }

}

/**
 *  \brief Löscht ein selektiertes Dokument.
 */
void MainWindow::deleteSheet(){

    int i = ui->ls_data->currentIndex().row();

    if(i>=0){
        ListItem* l =  this->listOfItems[i];

        Sheet* sheet = dynamic_cast<Sheet*>(l);

        if(sheet->dozent_id == this->currentuser->id){
            QMessageBox::StandardButton reply;
              reply = QMessageBox::question(this, "Sind Sie sich sicher?", "Dokument wirklich löschen?",
                                            QMessageBox::Yes|QMessageBox::No);

            if (reply == QMessageBox::Yes) {
                DatabaseService* db = new DatabaseService();
                bool result = db->deleteSheet(sheet);
                if(result){
                    this->listOfItems.erase(std::remove(this->listOfItems.begin(), this->listOfItems.end(), l));
                    this->updateListView(NULL);
                }
            }
        }else{
            QMessageBox::StandardButton reply;
              reply = QMessageBox::critical(this,"Error","Dokument wurde nicht von Ihnen erstellt.",QMessageBox::Yes);
        }

    }else{
        QMessageBox::StandardButton reply;
          reply = QMessageBox::critical(this,"Error","Bitte wählen Sie ein Dokunent aus der Liste aus.",QMessageBox::Yes);
    }


}
/**
 *  \brief Löscht ein selektierten User.
 */
void MainWindow::deleteDozent(){

    int i = ui->ls_data->currentIndex().row();

    if(i>=0){
        ListItem* l =  this->listOfItems[i];

        User* user = dynamic_cast<User*>(l);
        QMessageBox::StandardButton reply;
          reply = QMessageBox::question(this, "Sind Sie sich sicher?", "Dokument wirklich löschen?",
                                        QMessageBox::Yes|QMessageBox::No);

        if (reply == QMessageBox::Yes) {
            DatabaseService* db = new DatabaseService();
            bool result = db->deleteDozent(user);
            if(result){
                this->listOfItems.erase(std::remove(this->listOfItems.begin(), this->listOfItems.end(), l));
                this->updateListView(NULL);
            }
        }

    }else{
        QMessageBox::StandardButton reply;
          reply = QMessageBox::critical(this,"Error","Bitte wählen Sie ein Dokunent aus der Liste aus.",QMessageBox::Yes);
    }


}
/**
 *  \brief Öffnet das Fenster um sein Passwort zu ändern.
 */
void MainWindow::changePasswordDozent(){

    ChangePassword* c = new ChangePassword();
    c->setUpAndShow(this->currentuser);
    c->show();
}

/**
 *  \brief Ändert das Passwort des selektieren User.
 */
void MainWindow::changePasswordAdmin(){


    int i = ui->ls_data->currentIndex().row();

    if(i>=0){
        ListItem* l =  this->listOfItems[i];

        User* user = dynamic_cast<User*>(l);
        QMessageBox::StandardButton reply;
          reply = QMessageBox::question(this, "Sind Sie sich sicher?", "Passwort wirklich ändern?",
                                        QMessageBox::Yes|QMessageBox::No);

        if (reply == QMessageBox::Yes) {
            DatabaseService* db = new DatabaseService();

            PasswordService* spw = new PasswordService();
            QString newpw = spw->GetRandomPassword(8);
            db->changeDozentPassword(user,newpw,"",false);
        }

    }else{
        QMessageBox::StandardButton reply;
          reply = QMessageBox::critical(this,"Error","Bitte wählen Sie ein Dokunent aus der Liste aus.",QMessageBox::Yes);
    }

}
/**
 *  \brief Ladet alle Dokument mit den gegebenen Stichwörtern.
 */
void MainWindow::on_btn_search_clicked()
{
    QString keywords_without_space = ui->ip_keywords->text().replace(" ","");

    QStringList keywords;
    if(keywords_without_space.length()>0){
        keywords = keywords_without_space.split( "," );
    }

    DatabaseService* db = new DatabaseService();
    std::vector<ListItem*> datalist = db->loadSheets(keywords);
    this->listOfItems = datalist;
    this->updateListView(NULL);
}
/**
 *  \brief Resetet die Suche und ladet alle Dokumente.
 */
void MainWindow::on_btn_reset_clicked()
{
    QStringList keywords;
    DatabaseService* db = new DatabaseService();
    std::vector<ListItem*> datalist = db->loadSheets(keywords);
    this->listOfItems = datalist;
    this->updateListView(NULL);
}
/**
 *  \brief Logt den eingelogten User aus.
 */
void MainWindow::logout(){

    Login* l = new Login();
    l->show();
    this->close();


}
