#include "changepassword.h"
#include "ui_changepassword.h"
#include "Passwordservice.h"
#include "QMessageBox.h"
#include "DatabaseService.h";
ChangePassword::ChangePassword(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ChangePassword)
{
    ui->setupUi(this);
}

ChangePassword::~ChangePassword()
{
    delete ui;
}

void ChangePassword::setUpAndShow(User* u){
    this->user = u;
    this->show();
}

void ChangePassword::on_btn_ok_clicked()
{
    PasswordService *passwordService = new PasswordService;

        bool statusNew = passwordService->checkNewPassword(ui->ip_newpw->text(), ui->ip_newpw2->text());

        if (statusNew) {
            // Datenbank änderung vornehmen

            DatabaseService* db = new DatabaseService();
            bool result = db->changeDozentPassword(this->user,ui->ip_newpw->text(),ui->ip_currentpw->text(),true);
            this->close();
        }
        else {
            QMessageBox::StandardButton reply;
              reply = QMessageBox::warning(this,"Warnung","Passwörter stimmen nicht überein",QMessageBox::Yes);
        }
}

void ChangePassword::on_btn_exit_clicked()
{
    this->close();
}
